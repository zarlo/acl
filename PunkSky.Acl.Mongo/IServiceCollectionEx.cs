using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using PunkSky.Acl.Interface;

namespace PunkSky.Acl.Mongo;

/// <summary>
/// 
/// </summary>
public static class IServiceCollectionEx
{
    /// <summary>
    /// </summary>
    /// <param name="serviceProvider"></param>
    /// <param name="options"></param>
    /// <typeparam name="TUserId"></typeparam>
    /// <returns></returns>
    public static IServiceCollection AddMongoAclService<TUserId>(this IServiceCollection serviceProvider,
        Action<AclMongoOptions> options) where TUserId : class
    {
        var settings = new AclMongoOptions();
        options.Invoke(settings);
        return serviceProvider.AddMongoAclService<TUserId>(settings);
    }

    /// <summary>
    /// </summary>
    /// <param name="serviceProvider"></param>
    /// <param name="options"></param>
    /// <typeparam name="TUserId"></typeparam>
    /// <typeparam name="TGroupId"></typeparam>
    /// <returns></returns>
    public static IServiceCollection AddMongoAclService<TUserId>(this IServiceCollection serviceProvider,
        AclMongoOptions options) where TUserId : class
    {
        serviceProvider.AddOptions();
        serviceProvider.AddSingleton(_ => Options.Create(options));
        serviceProvider.AddScoped<IPermissionLoader<TUserId, ObjectId>, MongoPermissionLoader<TUserId>>();

        return serviceProvider.AddAclService<TUserId, ObjectId>();
    }
}
