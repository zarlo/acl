using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using PunkSky.Acl.Interface;

namespace PunkSky.Acl.Mongo;

/// <inheritdoc />
public class MongoPermissionLoader<TUserId> : IPermissionLoader<TUserId, ObjectId> 
    where TUserId : class
{
    private readonly IMongoCollection<AclModel<TUserId>> _collection;
    private readonly IMongoCollection<AclModel<ObjectId>> _groupCollection;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="options"></param>
    public MongoPermissionLoader(IOptions<AclMongoOptions> options)
    {
        _collection = options.Value.MongoDatabase.GetCollection<AclModel<TUserId>>(options.Value.CollectionName);
        _groupCollection = options.Value.MongoDatabase.GetCollection<AclModel<ObjectId>>(options.Value.CollectionName + "_group");
    }

    /// <inheritdoc />
    public async IAsyncEnumerable<PermissionNode> PermissionNodes(TUserId aclOwnerId,
        [EnumeratorCancellation] CancellationToken cancellationToken = default)
    {
        var filter = Builders<AclModel<TUserId>>.Filter.Eq(i => i.UserId, aclOwnerId);
        var data = await _collection.FindAsync(filter, cancellationToken: cancellationToken);
        var record = await data.FirstOrDefaultAsync(cancellationToken);
        if (record is null)
        {
            yield break;
        }

        foreach (var node in record.Permissions)
        {
            var permissionNode = new PermissionNode(node);
            if (permissionNode.GetNodes[0].ToLower() == "group")
            {
                await foreach (var groupNode in GroupPermissionNodes(ObjectId.Parse(permissionNode.GetNodes[1]), cancellationToken))
                {
                    yield return groupNode;
                }
            }

            yield return permissionNode;
        }
    }

    /// <inheritdoc />
    public async Task AddPermission(TUserId id, HashSet<string> permissions,
        CancellationToken cancellationToken = default)
    {
        var filter = Builders<AclModel<TUserId>>.Filter.Eq(i => i.UserId, id);
        var count = await _collection.CountDocumentsAsync(filter, cancellationToken: cancellationToken);
        if (count == 0)
        {
            var newModel = new AclModel<TUserId> { UserId = id, Permissions = permissions.ToList() };
            await _collection.InsertOneAsync(newModel, cancellationToken: cancellationToken);
            return;
        }

        var _ = await _collection.UpdateOneAsync(
            filter,
            Builders<AclModel<TUserId>>.Update.PushEach(i => i.Permissions, permissions),
            cancellationToken: cancellationToken
        );
    }

    /// <inheritdoc />
    public async Task RemovePermission(TUserId id, HashSet<string> permissions,
        CancellationToken cancellationToken = default)
    {
        var filter = Builders<AclModel<TUserId>>.Filter.Eq(i => i.UserId, id);
        var _ = await _collection.UpdateOneAsync(
            filter,
            Builders<AclModel<TUserId>>.Update.PullAll(i => i.Permissions, permissions),
            cancellationToken: cancellationToken
        );
    }

    /// <inheritdoc />
    public async Task RemovePermission(TUserId id, CancellationToken cancellationToken = default)
    {
        await _collection.DeleteOneAsync(i => i.UserId == id, cancellationToken);
    }

    public async IAsyncEnumerable<PermissionNode> GroupPermissionNodes(ObjectId aclOwnerId, CancellationToken cancellationToken = default)
    {
        var filter = Builders<AclModel<ObjectId>>.Filter.Eq(i => i.UserId, aclOwnerId);
        var data = await _groupCollection.FindAsync(filter, cancellationToken: cancellationToken);
        var record = await data.FirstOrDefaultAsync(cancellationToken);
        if (record is null)
        {
            yield break;
        }
        foreach (var permissionNode in record.Permissions.Select(node => new PermissionNode(node)))
        {
            yield return permissionNode;
        }
        
    }

    public async Task AddGroupPermission(ObjectId id, HashSet<string> permissions, CancellationToken cancellationToken = default)
    {
        var filter = Builders<AclModel<ObjectId>>.Filter.Eq(i => i.UserId, id);
        var count = await _groupCollection.CountDocumentsAsync(filter, cancellationToken: cancellationToken);
        if (count == 0)
        {
            var newModel = new AclModel<ObjectId> { UserId = id, Permissions = permissions.ToList() };
            await _groupCollection.InsertOneAsync(newModel, cancellationToken: cancellationToken);
            return;
        }

        var _ = await _groupCollection.UpdateOneAsync(
            filter,
            Builders<AclModel<ObjectId>>.Update.PushEach(i => i.Permissions, permissions),
            cancellationToken: cancellationToken
        );
    }

    public async Task RemoveGroupPermission(ObjectId id, HashSet<string> permissions, CancellationToken cancellationToken = default)
    {
        var filter = Builders<AclModel<ObjectId>>.Filter.Eq(i => i.UserId, id);
        var _ = await _groupCollection.UpdateOneAsync(
            filter,
            Builders<AclModel<ObjectId>>.Update.PullAll(i => i.Permissions, permissions),
            cancellationToken: cancellationToken
        );
    }

    public async Task RemoveGroupPermission(ObjectId id, CancellationToken cancellationToken = default)
    {
        await _groupCollection.DeleteOneAsync(i => i.UserId == id, cancellationToken);
    }
}
