using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace PunkSky.Acl.Mongo;

public class AclModel<T>
{
    [BsonId] public ObjectId Id { get; set; }

    /// <summary>
    /// the userId
    /// </summary>
    public required T UserId { get; set; }

    
    /// <summary>
    /// the permission list
    /// </summary>
    public required List<string> Permissions { get; set; }
}
