using MongoDB.Driver;

namespace PunkSky.Acl.Mongo;

/// <summary>
/// 
/// </summary>
public class AclMongoOptions
{
    /// <summary>
    /// 
    /// </summary>
    public IMongoDatabase MongoDatabase { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string CollectionName { get; set; }
}
