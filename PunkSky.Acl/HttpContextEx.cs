using Microsoft.AspNetCore.Http;

namespace PunkSky.Acl;


public static class HttpContextEx
{
    /// <summary>
    /// </summary>
    public const string UserIdKey = "PUNKSKY_USERID";

    /// <summary>
    /// </summary>
    public const string AclKey = "PUNKSKY_ACL";

    /// <summary>
    ///     get the current user id
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    public static object? GetUserId(this HttpContext context)
    {
        return context.Items[UserIdKey];
    }
}
