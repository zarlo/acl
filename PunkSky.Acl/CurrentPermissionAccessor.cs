using System;
using System.Collections.Generic;
using PunkSky.Acl.Interface;

namespace PunkSky.Acl;

/// <inheritdoc />
public class CurrentPermissionAccessor : ICurrentPermissionAccessor
{
    private readonly IEnumerable<PermissionNode>? _permissions;

    public CurrentPermissionAccessor(IEnumerable<PermissionNode>? permissions)
    {
        _permissions = permissions;
    }

    /// <inheritdoc />
    public IEnumerable<PermissionNode> GetCurrentPermissions()
    {
        return _permissions ?? Array.Empty<PermissionNode>();
    }
}
