using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using PunkSky.Acl.Interface;
using PunkSky.Acl.Nodes;
using StackExchange.Profiling;

namespace PunkSky.Acl.Attributes;

/// <summary>
/// </summary>
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
public class AclAttribute : Attribute, IAsyncAuthorizationFilter
{
    private readonly INode _node;

    /// <summary>
    ///     will just check it the user is logged in
    /// </summary>
    public AclAttribute()
    {
        _node = new LoggedInNode();
    }

    /// <summary>
    /// </summary>
    /// <param name="node"></param>
    public AclAttribute(INode node)
    {
        _node = node;
    }

    /// <summary>
    /// </summary>
    /// <param name="acl"></param>
    public AclAttribute(string acl)
    {
        _node = new LeafNode(acl);
    }

    /// <summary>
    /// </summary>
    /// <param name="acls"></param>
    public AclAttribute(params string[] acls)
    {
        _node = new AndNode(acls.Select(i => new LeafNode(i)));
    }


    /// <inheritdoc />
    public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
    {
        using (MiniProfiler.Current.Step("OnAuthorizationAsync"))
        {
            var currentPermission =
                context.HttpContext.RequestServices.GetRequiredService<ICurrentPermissionAccessor>();
            var userPermissions = currentPermission.GetCurrentPermissions();

            var authContext = new AclContext
            {
                HttpContext = context.HttpContext,
                UserPermissions = userPermissions?.ToList() ?? new List<PermissionNode>()
            };

            if (!await _node.Resolve(authContext, context.HttpContext.RequestAborted))
            {
                throw new ForbiddenException();
            }
        }
    }
}
