using System;
using System.Runtime.Serialization;

namespace PunkSky.Acl;

/// <inheritdoc />
public class ForbiddenException : Exception
{
    /// <inheritdoc />
    public ForbiddenException()
    {
    }

    /// <inheritdoc />
    protected ForbiddenException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

    /// <inheritdoc />
    public ForbiddenException(string? message) : base(message)
    {
    }

    /// <inheritdoc />
    public ForbiddenException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}
