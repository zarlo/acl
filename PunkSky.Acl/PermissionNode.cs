using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace PunkSky.Acl;

public class PermissionNode
{
    public PermissionNode(string permission)
    {
        GetNodes = permission.Split('.');
        GetPermission = permission;
    }

    public string GetPermission { get; private set; }

    public string[] GetNodes { get; private set; }

    /// <summary>
    ///     cast string to PermissionNode
    /// </summary>
    /// <param name="perm"></param>
    /// <returns></returns>
    public static implicit operator PermissionNode(string perm)
    {
        return new PermissionNode(perm);
    }

    /// <summary>
    ///     safe sting Substitute
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public PermissionNode Substitute(Dictionary<string, string> data)
    {
        foreach (var item in data)
        {
            var line = item.Value
                .Replace("{", "")
                .Replace("}", "")
                .Replace("*", "");

            for (var i = 0; i < GetNodes.Length; i++)
            {
                GetNodes[i] = GetNodes[i].Replace($@"{{{item.Key}}}", line);
            }
        }

        GetPermission = string.Join(".", GetNodes);

        GetNodes = GetPermission.Split('.');

        return this;
    }

    internal PermissionNode Inject(HttpContext? context)
    {
        if (context is null)
            return this;
        var data = new Dictionary<string, string>();
        if (context?.GetRouteData()?.Values != null)
        {
            foreach (var item in context.GetRouteData().Values)
            {
                data.Add(item.Key, item.Value?.ToString() ?? "");
            }
        }

        try
        {
            if (context?.Request.Query != null)
            {
                foreach (var item in context.Request.Query)
                {
                    data.Add(item.Key, item.Value);
                }
            }
        }
        catch (Exception)
        {
        }

        try
        {
            if (context?.Request.Form != null)
            {
                foreach (var item in context.Request.Form)
                {
                    data.Add(item.Key, item.Value);
                }
            }
        }
        catch (Exception)
        {
            // ignored
        }

        return Substitute(data);
    }

    /// <summary>
    /// </summary>
    /// <param name="permission"></param>
    /// <returns></returns>
    public bool IsMatch(PermissionNode permission)
    {
        var nodes = permission.GetNodes;
        // strings match so they are the same
        if (permission.GetPermission == GetPermission)
        {
            return true;
        }

        for (var i = 0; i < GetNodes.Length - 1; i++)
        {
            var nodeA = GetNodes[i];
            var nodeB = nodes[i];

            // wilde card logic
            if (nodeA == "*" || nodeB == "*")
            {
                if (nodes.Length - 1 == i)
                {
                    return true;
                }
            }

            else if (nodeA != nodeB)
            {
                return false;
            }
        }

        // match
        return false;
    }
}
