using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using PunkSky.Acl.Interface;

namespace PunkSky.Acl;

/// <summary>
///     service to manage Acls
/// </summary>
/// <typeparam name="TUserId"></typeparam>
public class AclService<TUserId, TGroupId>
{
    private readonly IPermissionLoader<TUserId, TGroupId> _permissionLoader;

    public AclService(IPermissionLoader<TUserId, TGroupId> permissionLoader)
    {
        _permissionLoader = permissionLoader;
    }

    /// <summary>
    /// adds permission to a user
    /// </summary>
    /// <param name="id"></param>
    /// <param name="acls"></param>
    /// <param name="cancellationToken"></param>
    public async Task AddPermissions(TUserId id, HashSet<string> acls, CancellationToken cancellationToken = default)
    {
        await _permissionLoader.AddPermission(id, acls, cancellationToken);
    }

    /// <summary>
    /// removes all of a users permissions
    /// </summary>
    /// <param name="id"></param>
    /// <param name="cancellationToken"></param>
    public async Task RemovePermissions(TUserId id, CancellationToken cancellationToken = default)
    {
        await _permissionLoader.RemovePermission(id, cancellationToken);
    }

    /// <summary>
    /// remove permissions
    /// </summary>
    /// <param name="id"></param>
    /// <param name="acls"></param>
    /// <param name="cancellationToken"></param>
    public async Task RemovePermissions(TUserId id, HashSet<string> acls, CancellationToken cancellationToken = default)
    {
        await _permissionLoader.RemovePermission(id, acls, cancellationToken);
    }

    /// <summary>
    /// gets the permissions of a user by id
    /// </summary>
    /// <param name="id"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<IEnumerable<PermissionNode>> GetPermissions(TUserId id, CancellationToken cancellationToken = default)
    {
        var permissions = new HashSet<PermissionNode>();
        await foreach (var item in _permissionLoader.PermissionNodes(id, cancellationToken))
        {
            permissions.Add(item);
        }

        return permissions;
    }
    
    
    /// <summary>
    /// adds permission to a user
    /// </summary>
    /// <param name="id"></param>
    /// <param name="acls"></param>
    /// <param name="cancellationToken"></param>
    public async Task AddGroupPermissions(TGroupId id, HashSet<string> acls, CancellationToken cancellationToken = default)
    {
        await _permissionLoader.AddGroupPermission(id, acls, cancellationToken);
    }

    /// <summary>
    /// removes all of a users permissions
    /// </summary>
    /// <param name="id"></param>
    /// <param name="cancellationToken"></param>
    public async Task RemoveGroup(TGroupId id, CancellationToken cancellationToken = default)
    {
        await _permissionLoader.RemoveGroupPermission(id, cancellationToken);
    }

    /// <summary>
    /// remove permissions
    /// </summary>
    /// <param name="id"></param>
    /// <param name="acls"></param>
    /// <param name="cancellationToken"></param>
    public async Task RemoveGroupPermissions(TGroupId id, HashSet<string> acls, CancellationToken cancellationToken = default)
    {
        await _permissionLoader.RemoveGroupPermission(id, acls, cancellationToken);
    }

    /// <summary>
    /// gets the permissions of a user by id
    /// </summary>
    /// <param name="id"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<IEnumerable<PermissionNode>> GetGroupPermissions(TGroupId id, CancellationToken cancellationToken = default)
    {
        var permissions = new List<PermissionNode>();
        await foreach (var item in _permissionLoader.GroupPermissionNodes(id, cancellationToken))
        {
            permissions.Add(item);
        }

        return permissions;
    }
    
}
