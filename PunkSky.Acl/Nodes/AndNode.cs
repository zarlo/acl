using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using PunkSky.Acl.Interface;

namespace PunkSky.Acl.Nodes;

public class AndNode : INode
{
    private readonly INode[] _nodes;

    public AndNode(params INode[] nodes)
    {
        _nodes = nodes;
    }

    public AndNode(IEnumerable<INode> nodes)
    {
        _nodes = nodes.ToArray();
    }


    /// <inheritdoc />
    public async Task<bool> Resolve(AclContext context, CancellationToken cancellationToken = default)
    {
        foreach (var node in _nodes)
        {
            var resolve = await node.Resolve(context, cancellationToken); 
            if (!resolve)
            {
                return false;
            }
        }

        return true;
    }
}
