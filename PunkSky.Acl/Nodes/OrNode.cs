using System.Threading;
using System.Threading.Tasks;
using PunkSky.Acl.Interface;

namespace PunkSky.Acl.Nodes;

public class OrNode : INode
{
    private readonly INode[] _nodes;

    public OrNode(params INode[] nodes)
    {
        _nodes = nodes;
    }

    public async Task<bool> Resolve(AclContext context, CancellationToken cancellationToken = default)
    {
        foreach (var node in _nodes)
        {
            if (await node.Resolve(context, cancellationToken))
            {
                return true;
            }
        }

        return false;
    }
}
