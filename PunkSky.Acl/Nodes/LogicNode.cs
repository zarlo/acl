using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using PunkSky.Acl.Interface;

namespace PunkSky.Acl.Nodes;

public class LogicNode : INode
{
    private readonly Func<AclContext, CancellationToken, Task<bool>> _callback;

    public LogicNode(Func<AclContext, CancellationToken, Task<bool>> callback)
    {
        _callback = callback;
    }
    


    /// <inheritdoc />
    public async Task<bool> Resolve(AclContext context, CancellationToken cancellationToken = default)
    {
        return await _callback(context, cancellationToken);
    }
}
