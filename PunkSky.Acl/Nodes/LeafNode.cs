using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using PunkSky.Acl.Interface;

namespace PunkSky.Acl.Nodes;

/// <summary>
///     end of the tree
/// </summary>
public class LeafNode : INode
{
    /// <summary>
    ///     end of the tree
    /// </summary>
    /// <param name="permissionNode"></param>
    public LeafNode(PermissionNode permissionNode)
    {
        Permission = permissionNode;
    }

    private PermissionNode Permission { get; }

    /// <inheritdoc />
    public Task<bool> Resolve(AclContext context, CancellationToken cancellationToken = default)
    {
        var permission = Permission.Inject(context.HttpContext);
        var isMatch = context.UserPermissions.Any(i =>
        {
            return permission.IsMatch(i);
        });
        return Task.FromResult(isMatch);
    }

    /// <summary>
    /// </summary>
    /// <param name="perm"></param>
    /// <returns></returns>
    public static implicit operator LeafNode(string perm)
    {
        return new LeafNode(perm);
    }

}
