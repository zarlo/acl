using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using PunkSky.Acl.Interface;

namespace PunkSky.Acl.Nodes;

public class LoggedInNode : INode
{
    public async Task<bool> Resolve(AclContext context, CancellationToken cancellationToken = default)
    {
        return context.UserPermissions.Any();
    }
}
