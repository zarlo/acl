using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using PunkSky.Acl.Interface;

namespace PunkSky.Acl;

public static class IServiceCollectionEx
{
    
    public static IServiceCollection AddAclService<TUserId, TGroupId>(this IServiceCollection serviceProvider)
    {
        serviceProvider.TryAddScoped<AclService<TUserId, TGroupId>>();
        serviceProvider.TryAddScoped<ICurrentPermissionAccessor>(provider =>
        {
            var userLoader = provider.GetRequiredService<IUserLoader<TUserId>>();
            var permissionLoader = provider.GetRequiredService<IPermissionLoader<TUserId, TGroupId>>();
            var httpContextAccessor = provider.GetRequiredService<IHttpContextAccessor>();

            var permissions = new List<PermissionNode>();

            if (httpContextAccessor?.HttpContext is not null)
            {
                Task.Run(async () =>
                {
                    var httpContext = httpContextAccessor.HttpContext;
                    var user = await userLoader.GetUserId(httpContext);
                    if (user is not null)
                    {
                        await foreach (var item in permissionLoader.PermissionNodes(user)
                                      )
                        {
                            permissions.Add(item);
                        }
                    }
                }).Wait();
            }

            return new CurrentPermissionAccessor(permissions);
        });

        return serviceProvider;
    }
}
