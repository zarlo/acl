using System.Collections.Generic;

namespace PunkSky.Acl.Interface;

public interface ICurrentPermissionAccessor
{
    public IEnumerable<PermissionNode> GetCurrentPermissions();
}
