using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace PunkSky.Acl.Interface;

/// <summary>
///     interface for loading Permission
/// </summary>
public interface IPermissionLoader<in TUserId, in TGroupId>
{
    /// <summary>
    ///     loads the acls for the given user
    /// </summary>
    /// <param name="aclOwnerId"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public IAsyncEnumerable<PermissionNode> PermissionNodes(TUserId aclOwnerId,
        [EnumeratorCancellation] CancellationToken cancellationToken = default);

    /// <summary>
    ///     add permissions to a user
    /// </summary>
    /// <param name="id"></param>
    /// <param name="permissions"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task AddPermission(TUserId id, HashSet<string> permissions, CancellationToken cancellationToken = default);

    /// <summary>
    ///     add permissions to a user
    /// </summary>
    /// <param name="id"></param>
    /// <param name="permissions"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task RemovePermission(TUserId id, HashSet<string> permissions,
        CancellationToken cancellationToken = default);

    /// <summary>
    ///     add permissions to a user
    /// </summary>
    /// <param name="id"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task RemovePermission(TUserId id, CancellationToken cancellationToken = default);
    
    
    /// <summary>
    ///     loads the acls for the given group
    /// </summary>
    /// <param name="aclOwnerId"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public IAsyncEnumerable<PermissionNode> GroupPermissionNodes(TGroupId aclOwnerId,
        [EnumeratorCancellation] CancellationToken cancellationToken = default);

    /// <summary>
    ///     add permissions to a group
    /// </summary>
    /// <param name="id"></param>
    /// <param name="permissions"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task AddGroupPermission(TGroupId id, HashSet<string> permissions, CancellationToken cancellationToken = default);

    /// <summary>
    ///     add permissions to a group
    /// </summary>
    /// <param name="id"></param>
    /// <param name="permissions"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task RemoveGroupPermission(TGroupId id, HashSet<string> permissions,
        CancellationToken cancellationToken = default);

    /// <summary>
    ///     add permissions to a group
    /// </summary>
    /// <param name="id"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task RemoveGroupPermission(TGroupId id, CancellationToken cancellationToken = default);
    
}
