using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PunkSky.Acl.Interface;

/// <summary>
/// </summary>
/// <typeparam name="TUserId"></typeparam>
public interface IUserLoader<TUserId>
{
    /// <summary>
    /// get the current user Id
    /// </summary>
    /// <param name="context"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task<TUserId?> GetUserId(HttpContext context, CancellationToken cancellationToken = default);

}
