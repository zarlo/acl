using System.Threading;
using System.Threading.Tasks;

namespace PunkSky.Acl.Interface;

/// <summary>
/// </summary>
public interface INode
{
    /// <summary>
    /// </summary>
    /// <param name="context"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task<bool> Resolve(
        AclContext context,
        CancellationToken cancellationToken = default
    );

}
