using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using PunkSky.Acl.Interface;

namespace PunkSky.Acl.Identity;

public class IdentityAclLoader<TUserId, TGroupId> : IPermissionLoader<TUserId, TGroupId>
    where TUserId : class
    where TGroupId : class
{
    private readonly IUserLoader<TUserId> _userLoader;
    private readonly UserManager<TUserId> _userManager;
    private readonly RoleManager<TGroupId> _roleManager;
    private readonly IHttpContextAccessor _httpContextAccessor;

    public IdentityAclLoader(IUserLoader<TUserId> userLoader,
        UserManager<TUserId> userManager, 
        RoleManager<TGroupId> roleManager,
        IHttpContextAccessor httpContextAccessor)
    {
        _userLoader = userLoader;
        _userManager = userManager;
        _roleManager = roleManager;
        _httpContextAccessor = httpContextAccessor;
    }

    /// <inheritdoc />
    public async IAsyncEnumerable<PermissionNode> PermissionNodes(TUserId aclOwnerId, [EnumeratorCancellation] CancellationToken cancellationToken = default)
    {
        if (_httpContextAccessor.HttpContext is null) yield break;
        var userId = await _userLoader.GetUserId(_httpContextAccessor.HttpContext, cancellationToken);
        if (userId is null) yield break;
        var roles = await _userManager.GetRolesAsync(userId);
        foreach (var role in roles)
        {
            yield return role;
        }
    }

    /// <inheritdoc />
    public async Task AddPermission(TUserId id, HashSet<string> permissions, CancellationToken cancellationToken = default)
    {
        await _userManager.AddToRolesAsync(id, permissions);
    }

    /// <inheritdoc />
    public async Task RemovePermission(TUserId id, HashSet<string> permissions, CancellationToken cancellationToken = default)
    {
        await _userManager.RemoveFromRolesAsync(id, permissions);
    }

    /// <inheritdoc />
    public async Task RemovePermission(TUserId id, CancellationToken cancellationToken = default)
    {
        await RemovePermission(id, (await _userManager.GetRolesAsync(id)).ToHashSet(), cancellationToken);
    }

    /// <inheritdoc />
    public async IAsyncEnumerable<PermissionNode> GroupPermissionNodes(TGroupId aclOwnerId, [EnumeratorCancellation] CancellationToken cancellationToken = default)
    {
        yield break;
    }

    /// <inheritdoc />
    public Task AddGroupPermission(TGroupId id, HashSet<string> permissions, CancellationToken cancellationToken = default)
    {
        return Task.CompletedTask;
    }

    /// <inheritdoc />
    public Task RemoveGroupPermission(TGroupId id, HashSet<string> permissions, CancellationToken cancellationToken = default)
    {
        return Task.CompletedTask;
    }

    /// <inheritdoc />
    public Task RemoveGroupPermission(TGroupId id, CancellationToken cancellationToken = default)
    {
        return Task.CompletedTask;
    }
}
