using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using PunkSky.Acl.Interface;

namespace PunkSky.Acl.Identity;

/// <summary>
/// loads user id from Identity 
/// </summary>
/// <typeparam name="TUserId"></typeparam>
public class IdentityUserLoader<TUserId>: IUserLoader<TUserId> where TUserId : class
{
    private readonly UserManager<TUserId> _userManager;
    private readonly IHttpContextAccessor _httpContextAccessor;

    public IdentityUserLoader(
        UserManager<TUserId> userManager,
        IHttpContextAccessor httpContextAccessor
        )
    {
        _userManager = userManager;
        _httpContextAccessor = httpContextAccessor;
    }


    /// <inheritdoc />
    public async Task<TUserId?> GetUserId(HttpContext context, CancellationToken cancellationToken = default)
    {
        if (_httpContextAccessor.HttpContext is null) return null;
        var user = _userManager.GetUserId(_httpContextAccessor.HttpContext.User);
        if (user is null) return null;
        return await _userManager.FindByIdAsync(user);
    }
}
