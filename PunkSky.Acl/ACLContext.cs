using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace PunkSky.Acl;

/// <summary>
///     context passed to note
/// </summary>
public class AclContext
{
    /// <summary>
    ///     the current HttpContext
    /// </summary>
    public required HttpContext? HttpContext { get; init; }

    /// <summary>
    ///     the current users PermissionNodes
    /// </summary>
    public required List<PermissionNode> UserPermissions { get; init; }
}
