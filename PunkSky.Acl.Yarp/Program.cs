using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Yarp.ReverseProxy.Transforms;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddReverseProxy()
    .LoadFromConfig(builder.Configuration.GetSection("ReverseProxy"))
    .AddTransforms(builder =>
    {
        builder.AddRequestTransform(async context =>
        {
            var httpContext = context.HttpContext;

            string? requestBody = "";
            httpContext.Request.Body.Position = 0;
            using (StreamReader sr = new StreamReader(httpContext.Request.Body, Encoding.UTF8, detectEncodingFromByteOrderMarks: true, leaveOpen: true))
            {
                requestBody = await sr.ReadToEndAsync();
            }

            var replacedContent = requestBody.Replace("%PLACEHOLDER%", "Hello");
            var requestContent = new StringContent(replacedContent, Encoding.UTF8, "application/json");
            httpContext.Request.Body = requestContent.ReadAsStream();
            httpContext.Request.ContentLength = httpContext.Request.Body.Length;
        });
    });
