using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Primitives;
using PunkSky.Acl.Nodes;
using Xunit;

namespace PunkSky.Acl.Test;


public class ContextTests
{

    [Fact]
    public async Task Context()
    {
        var features = new FeatureCollection();
        
        var requestForm = new Dictionary<string, StringValues>()
        {
            { "form_email", "null@punksky.com" },
            { "form_name", "cat" },
            { "form_bad", "{form_name}" }
        };
 
        var formCollection = new FormCollection(requestForm);
        var form = new FormFeature(formCollection);

        features.Set<IFormFeature>(form);

        var requestQuery = new Dictionary<string, StringValues>()
        {
            { "query_email", "null@punksky.com" },
            { "query_name", "cat" },
            { "query_bad", "{query_name}" }
        };

        var queryCollection = new QueryCollection(requestQuery);
        
        var query = new QueryFeature(queryCollection);

        features.Set<IQueryFeature>(query);
        
        var httpContext = new DefaultHttpContext(features);
        
        var aclContext = new AclContext()
        {
            HttpContext = httpContext,
            UserPermissions = new List<PermissionNode> { "test.user", "test.user.edit", "test.user.view", }
        };
        
        var tree0True = new AndNode(
            new LeafNode("test.user"),
            new LeafNode("test.user.view")
            );
        Assert.True(await tree0True.Resolve(aclContext));
        
        var tree0False = new AndNode(
            new LeafNode("test.user"),
            new LeafNode("test.user.cat")
        );
        Assert.False(await tree0False.Resolve(aclContext));
        
        
        var tree1True = new AndNode(
            new LeafNode("test.user"),
            new OrNode(
                new LeafNode("test.user.view"),
                new LeafNode("test.user.cat")
                )
            
        );
        Assert.True(await tree1True.Resolve(aclContext));
        
        var tree1False = new AndNode(
                new LeafNode("test.user.cat"),
            new OrNode(
                new LeafNode("test.user.view"),
            new LeafNode("test.user")
            )
            
        );
        Assert.False(await tree1False.Resolve(aclContext));
        
    }

}
