using System.Collections.Generic;
using System.Threading.Tasks;
using PunkSky.Acl.Nodes;
using Xunit;

namespace PunkSky.Acl.Test;


public class TreeTests
{

    [Fact]
    public async Task And()
    {

        var aclContext = new AclContext()
        {
            HttpContext = null,
            UserPermissions = new List<PermissionNode> { "test.user", "test.user.edit", "test.user.view", }
        };
        
        var tree0True = new AndNode(
            new LeafNode("test.user"),
            new LeafNode("test.user.view")
            );
        Assert.True(await tree0True.Resolve(aclContext));
        
        var tree0False = new AndNode(
            new LeafNode("test.user"),
            new LeafNode("test.user.cat")
        );
        Assert.False(await tree0False.Resolve(aclContext));
        
        
        var tree1True = new AndNode(
            new LeafNode("test.user"),
            new OrNode(
                new LeafNode("test.user.view"),
                new LeafNode("test.user.cat")
                )
            
        );
        Assert.True(await tree1True.Resolve(aclContext));
        
        var tree1False = new AndNode(
                new LeafNode("test.user.cat"),
            new OrNode(
                new LeafNode("test.user.view"),
            new LeafNode("test.user")
            )
            
        );
        Assert.False(await tree1False.Resolve(aclContext));
        
    }

}
